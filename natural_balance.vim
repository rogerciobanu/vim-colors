" Natural Balance 
"
" The theme uses termschool's structure 
" (https://github.com/marcopaganini/termschool-vim-theme).
" I just changed a few colors here and there to match my taste and
" make it easier for the eyes.
" 
" I was inspired by the following themes: 
" termschool(obvious), all-hallows-eve, rails-casts and idleFingers.


set background=dark
highlight clear

if exists("syntax_on")
  syntax reset
endif

let g:colors_name = "natural_balance"

hi Cursor ctermfg=16 ctermbg=146 cterm=NONE guifg=#182227 guibg=#9ea7a6 gui=NONE
hi Visual ctermfg=NONE ctermbg=59 cterm=NONE guifg=NONE guibg=#3f4b52 gui=NONE
hi CursorLine ctermfg=234 ctermbg=143 cterm=NONE guifg=NONE guibg=#2e373b gui=NONE
hi CursorColumn ctermfg=NONE ctermbg=23 cterm=NONE guifg=NONE guibg=#2e373b gui=NONE
hi ColorColumn ctermfg=NONE ctermbg=23 cterm=NONE guifg=NONE guibg=#2e373b gui=NONE
hi LineNr ctermfg=102 ctermbg=NONE cterm=NONE guifg=#84898c guibg=#2a343a gui=NONE
hi VertSplit ctermfg=59 ctermbg=59 cterm=NONE guifg=#252c31 guibg=#252c31 gui=NONE
hi MatchParen ctermfg=143 ctermbg=NONE cterm=NONE guifg=#dda790 guibg=NONE gui=NONE
hi StatusLine ctermfg=254 ctermbg=59 cterm=bold guifg=#f0f0f0 guibg=#575e61 gui=bold
hi StatusLineNC ctermfg=254 ctermbg=59 cterm=NONE guifg=#f0f0f0 guibg=#575e61 gui=NONE
hi Pmenu ctermfg=NONE ctermbg=59 cterm=NONE guifg=NONE guibg=#5f5f5f gui=NONE
hi PmenuSel ctermfg=NONE ctermbg=23 cterm=NONE guifg=NONE guibg=#005f5f gui=NONE
hi IncSearch ctermfg=16 ctermbg=107 cterm=NONE guifg=#182227 guibg=#8bb664 gui=NONE
hi Search ctermfg=16 ctermbg=107 cterm=NONE guifg=#000000 guibg=#00d75f gui=NONE
hi Directory ctermfg=180 ctermbg=NONE cterm=NONE guifg=#3c98d9 guibg=NONE gui=NONE
hi Folded ctermfg=146 ctermbg=16 cterm=NONE guifg=#9a9a9a guibg=#182227 gui=NONE

hi Normal ctermfg=254 ctermbg=234 cterm=NONE guifg=#f0f0f0 guibg=#252c31 gui=NONE
hi Boolean ctermfg=110 ctermbg=NONE cterm=NONE guifg=#3c98d9 guibg=NONE gui=NONE
hi Comment ctermfg=146 ctermbg=NONE cterm=NONE guifg=#9a9a9a guibg=NONE gui=italic
hi Constant ctermfg=110 ctermbg=NONE cterm=NONE guifg=#3c98d9 guibg=NONE gui=NONE
hi DiffAdd ctermfg=254 ctermbg=64 cterm=bold guifg=#f0f0f0 guibg=#43820d gui=bold
hi DiffDelete ctermfg=88 ctermbg=52 cterm=NONE guifg=#880708 guibg=#5f0000 gui=NONE
hi DiffChange ctermfg=254 ctermbg=23 cterm=NONE guifg=#f0f0f0 guibg=#1c3657 gui=NONE
hi DiffText ctermfg=254 ctermbg=61 cterm=bold guifg=#f0f0f0 guibg=#5f5faf gui=bold
hi ErrorMsg ctermfg=131 ctermbg=234 cterm=reverse guifg=#af5f5f guibg=#121212 gui=reverse
hi WarningMsg ctermfg=NONE ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi Identifier ctermfg=254 ctermbg=NONE cterm=NONE guifg=#99cf50 guibg=NONE gui=NONE
hi NonText ctermfg=146 ctermbg=234 cterm=NONE guifg=#767676 guibg=#232c31 gui=NONE
hi Number ctermfg=110 ctermbg=NONE cterm=NONE guifg=#3c98d9 guibg=NONE gui=NONE
hi PreProc ctermfg=180 ctermbg=NONE cterm=NONE guifg=#dda790 guibg=NONE gui=NONE
hi Special ctermfg=143 ctermbg=NONE cterm=NONE guifg=#f0f0f0 guibg=NONE gui=NONE
hi SpecialKey ctermfg=95 ctermbg=234 cterm=NONE guifg=#875f5f guibg=#252c31 gui=NONE
hi Statement ctermfg=180 ctermbg=NONE cterm=NONE guifg=#dda790 guibg=NONE gui=NONE
hi String ctermfg=143 ctermbg=NONE cterm=NONE guifg=#8bb664 guibg=NONE gui=NONE
hi Title ctermfg=254 ctermbg=NONE cterm=NONE guifg=#f0f0f0 guibg=NONE gui=NONE
hi Todo ctermfg=146 ctermbg=NONE cterm=inverse,bold guifg=#9a9a9a guibg=NONE gui=inverse,bold,italic
hi Type ctermfg=254 ctermbg=NONE cterm=NONE guifg=#b5d8f6 guibg=NONE gui=NONE
hi Underlined ctermfg=NONE ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
" ruby
hi rubyPseudoVariable ctermfg=110 ctermbg=NONE cterm=NONE guifg=#b5d8f6 guibg=NONE gui=NONE
hi erubyDelimiter ctermfg=254 ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi rubyStringDelimiter ctermfg=143 ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi rubyInterpolationDelimiter ctermfg=254 ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
" html
hi htmlTag ctermfg=250 ctermbg=NONE cterm=NONE guifg=#89bdff guibg=NONE gui=NONE
hi htmlEndTag ctermfg=250 ctermbg=NONE cterm=NONE guifg=#89bdff guibg=NONE gui=NONE
hi htmlTagName ctermfg=223 ctermbg=NONE cterm=NONE guifg=#89bdff guibg=NONE gui=NONE
hi htmlSpecialTagName ctermfg=223 ctermbg=NONE cterm=NONE guifg=#89bdff guibg=NONE gui=NONE
hi htmlArg ctermfg=250 ctermbg=NONE cterm=NONE guifg=#89bdff guibg=NONE gui=NONE
hi htmlItalic ctermfg=NONE ctermbg=234 cterm=NONE guifg=#89bdff guibg=NONE gui=NONE
" js
" hi javaScript ctermfg=254 ctermbg=NONE cterm=NONE guifg=#99cf50 guibg=NONE gui=NONE
hi javaScriptFunction ctermfg=180 ctermbg=NONE cterm=NONE guifg=#99cf50 guibg=NONE gui=NONE
hi javaScriptNumber ctermfg=110 ctermbg=NONE cterm=NONE guifg=#99cf50 guibg=NONE gui=NONE
hi javaScriptIdentifier ctermfg=110 ctermbg=NONE cterm=NONE guifg=#99cf50 guibg=NONE gui=NONE
hi javaScriptFuncDef ctermfg=254 ctermbg=NONE cterm=NONE guifg=#99cf50 guibg=NONE gui=NONE
hi javaScriptFuncKeyword ctermfg=180 ctermbg=NONE cterm=NONE guifg=#99cf50 guibg=NONE gui=NONE
hi javaScriptFuncArg ctermfg=254 ctermbg=NONE cterm=NONE guifg=#99cf50 guibg=NONE gui=NONE
hi javaScriptFuncComma ctermfg=254 ctermbg=NONE cterm=NONE guifg=#99cf50 guibg=NONE gui=NONE
hi javaScriptOpSymbols ctermfg=254 ctermbg=NONE cterm=NONE guifg=#99cf50 guibg=NONE gui=NONE
hi javaScriptParens ctermfg=254 ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi javaScriptEndColons ctermfg=254 ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi javaScriptNull ctermfg=110 ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi javaScriptDollar ctermfg=110 ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi javaScriptTemplate ctermfg=143 ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi javaScriptTemplateSubstitution ctermfg=254 ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi javaScriptGlobalObjects ctermfg=254 ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi javaScriptLogicSymbols ctermfg=254 ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi javaScriptDOMObjects ctermfg=138 ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi javaScriptBrowserObjects ctermfg=138 ctermbg=NONE cterm=NONE guifg=NONE guibg=NONE gui=NONE
hi Label ctermfg=254 ctermbg=NONE cterm=NONE guifg=#8bb664 guibg=NONE gui=NONE
" css
hi cssTagName ctermfg=223 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
hi cssPseudoClassId ctermfg=143 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
hi cssClassName ctermfg=143 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
hi cssPseudoClassLang ctermfg=143 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
hi cssIdentifier ctermfg=143 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
hi cssAttrRegion ctermfg=110 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
hi cssInclude ctermfg=254 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
hi cssIncludeKeyword ctermfg=180 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
hi cssMediaType ctermfg=143 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
hi cssAttrComma ctermfg=254 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
hi cssVendor ctermfg=254 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
hi cssFunctionName ctermfg=110 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
hi sassClass ctermfg=143 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
hi sassId ctermfg=143 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
" coffeeScript
hi coffeeOperator ctermfg=254 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
hi coffeeParens ctermfg=254 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
hi coffeeCurlies ctermfg=254 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
hi coffeeBlock ctermfg=254 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
" java
hi JavaDocComment ctermfg=146 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
hi JavaComment ctermfg=146 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
hi JavaCommentTitle ctermfg=146 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
hi JavaDocTags ctermfg=146 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
hi JavaDocParam ctermfg=146 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
hi JavaCommentStar ctermfg=146 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
hi JavaDocSeeTagParam ctermfg=146 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
hi JavaStorageClass ctermfg=180 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
hi JavaStorageClass ctermfg=180 ctermbg=NONE cterm=NONE guifg=#bcdbff guibg=NONE gui=NONE
" xml
hi xmlTag ctermfg=254 ctermbg=NONE cterm=NONE guifg=#89bdff guibg=NONE gui=NONE
hi xmlString ctermfg=254 ctermbg=NONE cterm=NONE guifg=#89bdff guibg=NONE gui=NONE
hi xmlEndTag ctermfg=254 ctermbg=NONE cterm=NONE guifg=#89bdff guibg=NONE gui=NONE
hi xmlTagName ctermfg=254 ctermbg=NONE cterm=NONE guifg=#89bdff guibg=NONE gui=NONE
" hi xmlAttrib ctermfg=146 ctermbg=NONE cterm=NONE guifg=#89bdff guibg=NONE gui=NONE
" hi xmlEqual ctermfg=146 ctermbg=NONE cterm=NONE guifg=#89bdff guibg=NONE gui=NONE
" php
hi Delimiter ctermfg=130 ctermbg=NONE cterm=NONE guifg=#89bdff guibg=NONE gui=NONE
hi phpParent ctermfg=254 ctermbg=NONE cterm=NONE guifg=#89bdff guibg=NONE gui=NONE
hi phpDocTags ctermfg=146 ctermbg=NONE cterm=NONE guifg=#89bdff guibg=NONE gui=NONE
hi phpVarSelector ctermfg=254 ctermbg=NONE cterm=NONE guifg=#89bdff guibg=NONE gui=NONE
hi phpIdentifier ctermfg=254 ctermbg=NONE cterm=NONE guifg=#89bdff guibg=NONE gui=NONE
hi phpConstant ctermfg=254 ctermbg=NONE cterm=NONE guifg=#89bdff guibg=NONE gui=NONE
hi phpSpecialFunction ctermfg=254 ctermbg=NONE cterm=NONE guifg=#89bdff guibg=NONE gui=NONE
hi phpStructure ctermfg=180 ctermbg=NONE cterm=NONE guifg=#89bdff guibg=NONE gui=NONE
hi phpStorageClass ctermfg=180 ctermbg=NONE cterm=NONE guifg=#89bdff guibg=NONE gui=NONE

" json
hi jsonKeyword ctermfg=143 ctermbg=NONE cterm=NONE guifg=#89bdff guibg=NONE gui=NONE

